import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:local_push/local_push.dart';
import 'package:package_info/package_info.dart';
import 'package:sysviews/sysviews.dart';
import 'package:device_info/device_info.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _platform = "";
  bool isLoaded = false;
  PackageInfo _packageInfo;
  AndroidDeviceInfo _androidDeviceInfo;
  TextEditingController textController;
  String _packageName = "";

  @override
  void initState() {
    super.initState();
    initPlatformState();
    textController = new TextEditingController();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await LocalPush.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    _androidDeviceInfo = await deviceInfo.androidInfo;
    var platform = _androidDeviceInfo.brand.toLowerCase();
    _packageInfo = await PackageInfo.fromPlatform();
    IntentNames.getInstance().init(_packageInfo, _androidDeviceInfo);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = _androidDeviceInfo.version.release;
      _platform = platform;
      isLoaded = true;
      _packageName = _packageInfo.packageName;
    });
  }

  var time = 0;
  var enableLocalPush = false;

  @override
  Widget build(BuildContext context) {
    return isLoaded
        ? MaterialApp(
            home: Scaffold(
              body: Builder(
                  builder: (context) => Material(
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text("平台:$_platform" +
                                  ",version:" +
                                  _platformVersion),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: TextField(
                                      maxLines: 1,
                                      controller: textController,
                                    ),
                                  ),
                                  RaisedButton(
                                    child: Text("调转"),
                                    onPressed: () {
                                      Sysviews.openIntentSetting(
                                          textController.text);
                                    },
                                  ),
                                ],
                              ),
                              RaisedButton(
                                onPressed: () async {
                                  var timeData = await showTimePicker(
                                      context: context,
                                      initialTime: new TimeOfDay.now());
                                  print(timeData.toString());
                                  var time =
                                      timeData.hour * 60 + timeData.minute;
                                  var timeStr = "";
                                  for (int i = 0; i < 1000; i++) {
                                    timeStr =
                                        timeStr + (time + i * 30).toString();
                                    if (i != 9) {
                                      timeStr = timeStr + ",";
                                    }
                                  }
//              return;
                                  LocalPush.updateData(
                                      '[{"schedulerID":1770,"userID":6458,"boxID":506663,"category":"pill-baojian","title":"第二个计划","picture":"","alarmUser":true,"notifySupervisor":true,"dattern":{"startDate":18219,"endDate":18249,"state":"PillPlanState.customScheme","scheme":"CustomPlanState.jianGeShiJian","weekDays":[true,true,true,true,true,true,true],"periodDays":1,"durationDays":1,"timers":[$timeStr, 920,998]},"locale":"zh_CN"},'
                                          '{"schedulerID":1770,"userID":6458,"boxID":506663,"category":"pill-baojian","title":"第一个计划","picture":"","alarmUser":true,"notifySupervisor":true,"dattern":{"startDate":18219,"endDate":18249,"state":"PillPlanState.customScheme","scheme":"CustomPlanState.jianGeShiJian","weekDays":[true,true,true,true,true,true,true],"periodDays":1,"durationDays":1,"timers":[$timeStr,920,998]},"locale":"zh_CN"}]',
                                      "[]",
                                      "[]",
                                      "[]");
                                },
                                child: Text("设置时间"),
                              ),

                              _platformBtns()

//                        Row(
//                          children: <Widget>[
//                            Text("开启本地推送："),
//                            Checkbox(
//                              value: enableLocalPush,
//                              onChanged: (value) {
//                                setState(() {
//                                  enableLocalPush = value;
//                                });
//                                LocalPush.enableLocalPush(enableLocalPush);
//                              },
//                            )
//                          ],
//                        )
//                              RaisedButton(
//                                onPressed: () async {
//                                  Sysviews.openIntentSetting(
//                                      await IntentNames.getInstance()
//                                          .getNotificationUri());
//                                },
//                                child: Text("通知"),
//                              ),
//                              RaisedButton(
//                                onPressed: () async {
//                                  Sysviews.openIntentSetting(
//                                      await IntentNames.getInstance()
//                                          .getSelfLaunch());
//                                },
//                                child: Text("自启动"),
//                              ),
//                              _platform == "vivo"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getAboutLaunch());
//                                      },
//                                      child: Text("关联启动"),
//                                    )
//                                  : Container(),
//                              RaisedButton(
//                                onPressed: () async {
//                                  Sysviews.openIntentSetting(
//                                      await IntentNames.getInstance()
//                                          .getBatteryOptimization());
//                                },
//                                child: Text("电池优化"),
//                              ),
//                              _platform == "xiaomi"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getBatterySaving());
//                                      },
//                                      child: Text("电池省电策略"),
//                                    )
//                                  : Container(),
//                              _platform == "xiaomi"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getBatterySettingSuoPing());
//                                      },
//                                      child: Text("电池设置锁屏"),
//                                    )
//                                  : Container(),
//                              _platform == "xiaomi" || _platform == "oppo"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getBatterySettingShuiMian());
//                                      },
//                                      child: Text("电池设置睡眠"),
//                                    )
//                                  : Container(),
//                              _platform == "oppo"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getBatterySettingShuiMian());
//                                      },
//                                      child: Text("耗电保护"),
//                                    )
//                                  : Container(),
//                              _platform == "vivo"
//                                  ? RaisedButton(
//                                      onPressed: () async {
//                                        Sysviews.openIntentSetting(
//                                            await IntentNames.getInstance()
//                                                .getGaoHaoDian());
//                                      },
//                                      child: Text("后台高耗电"),
//                                    )
//                                  : Container(),
                            ],
                          ),
                        ),
                      )),
            ),
          )
        : Container();
  }

  Widget _platformBtns() {
    var v = int.parse(_platformVersion.substring(0, 1));
    switch (_platform) {
      case "xiaomi":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "自启动", IntentNames.getInstance().getSelfLaunch()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            platformSettingBtn(
                "电池省电策略", IntentNames.getInstance().getBatterySaving()),
            platformSettingBtn(
                "电池设置锁屏", IntentNames.getInstance().getBatterySettingSuoPing()),
            platformSettingBtn(
                "电池设置睡眠", IntentNames.getInstance().getBatterySettingShuiMian())
          ],
        );
      case "huawei":
      case "honor":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "自启动", IntentNames.getInstance().getSelfLaunch()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
          ],
        );
      case "oppo":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "自启动", IntentNames.getInstance().getSelfLaunch()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            platformSettingBtn("设置", IntentNames.getInstance().getSetting()),
//            platformSettingBtn(
//                "电池优化", IntentNames.getInstance().getBatteryOptimization())
          ],
        );
      case "vivo":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn("自启动0",
                "#Intent;launchFlags=0x10000000;component=com.iqoo.secure/.ui.phoneoptimize.BgStartUpManager;end"),
            platformSettingBtn(
                "自启动", IntentNames.getInstance().getSelfLaunch()),
            platformSettingBtn(
                "关联启动", IntentNames.getInstance().getAboutLaunch()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            platformSettingBtn(
                "后台高耗电", IntentNames.getInstance().getGaoHaoDian())
          ],
        );
      case "meizu":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            (_platformVersion == "6.0")
                ? platformSettingBtn(
                    "自启动", IntentNames.getInstance().getSelfLaunch())
                : platformSettingBtn(
                    "后台管理", IntentNames.getInstance().getHouTaiGuanLi()),
            (_platformVersion == "6.0")
                ? platformSettingBtn(
                    "待机省电", IntentNames.getInstance().getShengDianYouHua())
                : platformSettingBtn(
                    "智能休眠", IntentNames.getInstance().getZhiNengXiuMian())
          ],
        );
      case "oneplus":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            (_platformVersion == '9' || _platformVersion == '10')
                ? Container()
                : platformSettingBtn(
                    "自启动", IntentNames.getInstance().getSelfLaunch()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            _platformVersion == '9'
                ? platformSettingBtn(
                    "电池", IntentNames.getInstance().getDianChi())
                : Container(),
          ],
        );
      case "lenovo":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            platformSettingBtn(
                "自启动", IntentNames.getInstance().getSelfLaunch()),
            v == 6
                ? platformSettingBtn("智能省电0",
                    "#Intent;action=net.kigis.app.one2;component=com.lenovo.powersetting/.ui.Settings\$SmartPowerFragmentActivity;S.package_name=net.kigis.app.one2;end")
                : platformSettingBtn("省电",
                    "#Intent;action=net.kigis.app.one2;component=com.android.settings/.Settings\$PowerUsageSummaryActivity;S.package_name=net.kigis.app.one2;end"),
            v == 6
                ? platformSettingBtn("高耗电0",
                    "#Intent;action=net.kigis.app.one2;component=com.lenovo.powersetting/.ui.Settings\$HighPowerApplicationsActivity;S.package_name=net.kigis.app.one2;end")
                : platformSettingBtn("电池",
                    "#Intent;action=net.kigis.app.one2;component=com.android.settings/.Settings\$PowerUsageSummaryActivity;S.package_name=net.kigis.app.one2;end"),
          ],
        );
      case "nubia":
        return Column(
          children: <Widget>[
            platformSettingBtn(
                "通知", IntentNames.getInstance().getNotificationUri()),
            platformSettingBtn(
                "省电优化", IntentNames.getInstance().getBatteryOptimization()),
            v == 8
                ? platformSettingBtn("自启动只能跳设置",
                    "#Intent;launchFlags=0x10000000;component=com.android.settings/.Settings;end")
                : platformSettingBtn("自启动",
                    "#Intent;launchFlags=0x10000000;component=cn.nubia.security2/cn.nubia.security.appmanage.selfstart.u Ri.SelfStartActivity;end"),
            platformSettingBtn(
                "电池优化", IntentNames.getInstance().getBatteryOptimization()),
            v == 8
                ? platformSettingBtn("省电管理",
                    "#Intent;component=cn.nubia.powersaving/.ui.PowerManageActivity;end")
                : Container(),
            v == 6
                ? platformSettingBtn("耗电保护_白名单(只能跳电池)",
                    "#Intent;action=net.kigis.app.one2;component=cn.nubia.security2/cn.nubia.security.powermanage.ui.PowerManageActivity;S.package_name=net.kigis.app.one2;end")
                : Container(),
            v == 6
                ? platformSettingBtn("后台保护_白名单(只能跳电池)",
                    "#Intent;action=net.kigis.app.one2;component=cn.nubia.security2/cn.nubia.security.powermanage.ui.PowerManageActivity;S.package_name=net.kigis.app.one2;end")
                : Container()
          ],
        );
      case "samsung":
        return Column(children: <Widget>[
          platformSettingBtn(
              "通知", IntentNames.getInstance().getNotificationUri()),
          platformSettingBtn(
              "省电优化", IntentNames.getInstance().getBatteryOptimization()),
          platformSettingBtn("自启动", IntentNames.getInstance().getSelfLaunch()),
        ]);
      case "nokia":
        return Column(children: <Widget>[
          platformSettingBtn(
              "通知", IntentNames.getInstance().getNotificationUri()),
          platformSettingBtn(
              "省电优化", IntentNames.getInstance().getBatteryOptimization()),
          platformSettingBtn("自启动", IntentNames.getInstance().getSelfLaunch()),
        ]);
    }
    return Container();
  }

  Widget platformSettingBtn(
    String name,
    String setting,
  ) {
    return RaisedButton(
      onPressed: () async {
        Sysviews.openIntentSetting(setting);
      },
      child: Text(name),
    );
  }
}

class IntentNames {
  static IntentNames intentNames;
  PackageInfo _packageInfo;
  AndroidDeviceInfo _androidDeviceInfo;
  String _platform;
  String _packageName;
  String _platformVersion;

  static IntentNames getInstance() {
    if (intentNames == null) {
      intentNames = new IntentNames();
    }
    return intentNames;
  }

  void init(PackageInfo packageInfo, AndroidDeviceInfo androidDeviceInfo) {
    this._packageInfo = packageInfo;
    this._androidDeviceInfo = androidDeviceInfo;
    _platform = androidDeviceInfo.brand.toLowerCase();
    _packageName = packageInfo.packageName;
    _platformVersion = androidDeviceInfo.version.release;
  }

  String getNotificationUri() {
//    if (_platform == "meizu") {
//      return "#Intent;action=" +
//          _packageName +
//          ";component=com.meizu.safe/.permission.PermissionMainActivity;S.package_name=" +
//          _packageName +
//          ";end";
//    }
    return "package:net.kigis.app.one2#Intent;action=android.settings.APPLICATION_DETAILS_SETTINGS;end";
//    }
//    return "";
  }

  String getBatteryOptimization() {
    return "package:net.kigis.app.one2##Intent;action=android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;end";
  }

  String getSelfLaunch() {
    var v = int.parse(_platformVersion.substring(0, 1));

    if (_platform == "honor" || _platform == "huawei") {
      return "#Intent;launchFlags=0x10000000;component=com.huawei.systemmanager/.startupmgr.ui.StartupNormalAppListActivity;end";
    }
    if (_platform == "xiaomi") {
      return "#Intent;launchFlags=0x10000000;component=com.miui.securitycenter/com.miui.permcenter.autostart.AutoStartManagementActivity;end";
    }
    if (_platform == 'oppo') {
      return "#Intent;launchFlags=0x10000000;component=com.coloros.safecenter/com.coloros.privacypermissionsentry.PermissionTopActivity;end";
    }
    if (_platform == 'vivo') {
      if (v >= 9) {
        return "#Intent;launchFlags=0x10000000;component=com.vivo.permissionmanager/.activity.BgStartUpManagerActivity;end";
      } else if (v > 6) {
        return "#Intent;launchFlags=0x10000000;component=com.vivo.permissionmanager/.activity.PurviewTabActivity;end";
      } else {
        if (_platformVersion == "6.0") {
          return "#Intent;launchFlags=0x10000000;component=com.iqoo.secure/.MainActivity;end";
        } else {
          return "#Intent;launchFlags=0x10000000;component=com.vivo.permissionmanager/.activity.PurviewTabActivity;end";
        }
      }
    }
    if (_platform == 'meizu') {
      return "#Intent;launchFlags=0x10000000;component=com.meizu.safe/.permission.PermissionMainActivity;end";
    }
    if (_platform == 'samsung') {
      if (_platformVersion == "6.0.1")
        return "#Intent;launchFlags=0x10000000;component=com.samsung.android.sm/.app.dashboard.SmartManagerDashBoardActivity;end";
      else
        return "#Intent;launchFlags=0x10000000;component=com.samsung.android.sm_cn/com.samsung.android.sm.ui.cstyleboard.SmartManagerDashBoardActivity;end";
    }
//    if (_platform == 'samsung') {
//      if (_platformVersion == "6.0.1")
//        return "#Intent;launchFlags=0x10000000;component=com.samsung.android.sm/.app.dashboard.SmartManagerDashBoardActivity;end";
//      else
//        return "#Intent;launchFlags=0x10000000;component=com.samsung.android.sm_cn/com.samsung.android.sm.ui.cstyleboard.SmartManagerDashBoardActivity;end";
//    }
    if (_platform == 'oneplus') {
      return "#Intent;launchFlags=0x10000000;component=com.oneplus.security/.autorun.AutorunMainActivity;end";
    }
    if (_platform == 'lenovo') {
      return v == 6
          ? "#Intent;launchFlags=0x10000000;component=com.lenovo.security/.purebackground.PureBackgroundActivity;end"
          : "#Intent;launchFlags=0x10000000;component=com.zui.safecenter/com.lenovo.safecenter.MainTab.LeSafeMainActivity;end";
    }
    if (_platform == 'nubia') {
      return "#Intent;launchFlags=0x10000000;component=com.lenovo.security/.purebackground.PureBackgroundActivity;end";
    }
    if (_platform == "nokia") {
      return "#Intent;component=com.evenwell.powersaving.g3/.exception.PowerSaverExceptionActivity;end";
    }
//    return
//      "
//      ";
  }

  String getAboutLaunch() {
    if (_platform == 'vivo') {
      return "#Intent;launchFlags=0x10000000;component=com.vivo.appfilter/.activity.StartupManagerActivityRom30;end";
    }
    return "";
  }

  String getBatterySaving() {
    if (_platform == "xiaomi") {
      return "#Intent;action=net.kigis.app.one2;component=com.miui.powerkeeper/.ui.HiddenAppsConfigActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getBatterySettingSuoPing() {
    if (_platform == "xiaomi") {
      return "#Intent;action=net.kigis.app.one2;component=com.miui.securitycenter/com.miui.powercenter.PowerSettings;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getBatterySettingShuiMian() {
    if (_platform == "xiaomi") {
      return "#Intent;action=net.kigis.app.one2;component=com.miui.powerkeeper/.ui.ScenarioPowerSavingActivity;S.package_name=net.kigis.app.one2;end";
    } else if (_platform == "oppo") {
      return "#Intent;action=net.kigis.app.one2;component=com.android.settings/com.oppo.settings.SettingsActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getSetting() {
    if (_platform == "oppo") {
      return "#Intent;component=com.android.settings/com.oppo.settings.SettingsActivity;end";
    }
    return "";
  }

  String getGaoHaoDian() {
    if (_platform == "vivo") {
      return "#Intent;launchFlags=0x10000000;component=com.iqoo.powersaving/.PowerSavingManagerActivity;end";
    }
    return "";
  }

  String getHaoDianbaoHuInOppo6Version() {
    if (_platform == "oppo") {
      return "#Intent;action=net.kigis.app.one2;component=com.miui.powerkeeper/.ui.ScenarioPowerSavingActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getHouTaiGuanLi() {
    if (_platform == "meizu") {
      return "#Intent;action=net.kigis.app.one2;component=com.meizu.safe/.permission.SmartBGActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getZhiNengXiuMian() {
    if (_platform == "meizu") {
      return "#Intent;action=net.kigis.app.one2;component=com.meizu.battery/.setting.PowerSetupActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getZhiNengShengDian() {
    if (_platform == "lenovo") {
      return "#Intent;action=net.kigis.app.one2;component=com.lenovo.powersetting/.ui.Settings\$SmartPowerFragmentActivity;S.package_name=net.kigis.app.one2;end";
    }
    return "";
  }

  String getShengDianYouHua() {
    if (_platform == "meizu") {
      return "#Intent;launchFlags=0x10000000;component=com.meizu.safe/.powerui.PowerAppPermissionActivity;end";
    }
    return "";
  }

  String getDianChi() {
    if (_platform == "oneplus") {
      return "#Intent;launchFlags=0x10000000;component=com.android.settings/.Settings\$PowerUsageSummaryActivity;end";
    }
    return "";
  }
}

// "package:net.kigis.app.one2#Intent;action=android.settings.APPLICATION_DETAILS_SETTINGS;end"
// package:net.kigis.app.one2##Intent;action=android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;end
// #Intent;action=net.kigis.app.one2;component=com.miui.powerkeeper/.ui.HiddenAppsConfigActivity;S.package_name=net.kigis.app.one2;end
//电池设置 #Intent;action=net.kigis.app.one2;component=com.miui.securitycenter/com.miui.powercenter.PowerSettings;S.package_name=net.kigis.app.one2;end
//"#Intent;action=com.miui.powerkeeper/.ui.ScenarioPowerSavingActivity;S.package_name=net.kigis.app.one2;end"

//"#Intent;action=net.kigis.app.one2;component=com.lenovo.powersetting/.ui.Settings\$SmartPowerFragmentActivity;S.package_name=net.kigis.app.one2;end"
//自启动 "#Intent;action=net.kigis.app.one2;component=cn.nubia.security2/cn.nubia.security.appmanage.selfstart.ui.SelfStartActivity;S.package_name=net.kigis.app.one2;end"
//电池页面 "#Intent;action=net.kigis.app.one2;component=com.evenwell.powersaving.g3/.exception.PowerSaverExceptionActivity;S.package_name=net.kigis.app.one2;end"
