package com.example.local_push;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;

import com.example.local_push.data.DataStore;
import com.example.local_push.service.MyIntentJobService;
import com.example.local_push.service.PushService;
import com.example.local_push.util.CalendarUtil;
import com.example.local_push.util.JobUtil;
import com.example.local_push.util.LogUtil;
import com.example.local_push.util.NotificationChannels;
import com.example.local_push.util.PermissionUtil;
import com.example.local_push.util.ServiceUtil;
import com.example.local_push.worker.WorkerUtil;

import java.util.Calendar;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static android.content.Context.POWER_SERVICE;

/**
 * LocalPushPlugin
 */
public class LocalPushPlugin implements MethodCallHandler {
    /**
     * Plugin registration.
     */
    public static Activity activity;

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "local_push");
        channel.setMethodCallHandler(new LocalPushPlugin());
        activity = registrar.activity();
        DataStore.getInstance().init(activity);
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        LogUtil.print("onMethodCall:" + call.method);
        if (call.method.equals("getPlatformVersion")) {
            NotificationChannels.show(LocalPushPlugin.activity, "测试弹出", "asjdoasjdopaj");
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("updateFreshTip")) {
            updateFreshTip(call.argument("message").toString());
        } else if (call.method.equals("enableLocalPush")) {
            enableLocalPush(call.argument("enableLocalPush").toString());
            result.success(null);
        } else if (call.method.equals("updateSchedulerTip")) {
            LogUtil.print("updateSchedulerTip");
            updateBoxes(call.argument("boxes").toString());
            updateSchedulerTip(call.argument("schedulers").toString(), call.argument("tasks").toString());
//            PermissionUtil.requireAutostart(activity);
        } else if (call.method.equals("updateData")) {
            updateBoxeAndTasks(call.argument("boxes").toString(), call.argument("tasks").toString());
            updateDatas(call.argument("schedulers").toString(), call.argument("freshTasks").toString());
//            PermissionUtil.requireAutostart(activity);
        }
    }

    private void updateFreshTip(String msg) {
        DataStore.getInstance().init(activity);
        LogUtil.print("updateFreshTip:" + msg);
    }

    private void updateSchedulerTip(String schedulersStr, String tasksStr) {
        LogUtil.print("schedulersStr:" + schedulersStr);
        LogUtil.print("tasksStr:" + tasksStr);

        Calendar calendar = Calendar.getInstance();
        DataStore.getInstance().init(activity);
        DataStore.getInstance().setSchedulers(schedulersStr);
        DataStore.getInstance().setTasks(tasksStr);
//        ServiceUtil.startService(activity, PushService.class);
        WorkerUtil.start(activity);
    }

    private void enableLocalPush(String str) {
        LogUtil.print("****** enableLocalPush：" + str);
        if (str.equals("1")) {

        } else {
            DataStore.getInstance().init(activity);
            DataStore.getInstance().setSchedulers("[]");
            DataStore.getInstance().setTasks("[]");
            DataStore.getInstance().setFreshTasks("[]");

            WorkerUtil.close(activity);
            if (HandlerProgress._handlerProgress != null) {
                HandlerProgress._handlerProgress.stop();
            }
        }

    }

    private void updateBoxes(String boxes) {
        DataStore.getInstance().init(activity);
        DataStore.getInstance().setBoxes(boxes);
    }

    private void updateBoxeAndTasks(String boxes, String tasks) {
        DataStore.getInstance().init(activity);
        DataStore.getInstance().setBoxes(boxes);
        DataStore.getInstance().setTasks(tasks);
    }

    private void updateDatas(String schedulersStr, String freshTasks) {
        DataStore.getInstance().init(activity);
        DataStore.getInstance().setSchedulers(schedulersStr);
        DataStore.getInstance().setFreshTasks(freshTasks);

        WorkerUtil.start(activity);
    }
}
