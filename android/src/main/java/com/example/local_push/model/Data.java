package com.example.local_push.model;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {
    List<String> pictures;

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    @Override
    public String toString() {
        return "Data{" +
                "pictures=" + pictures +
                '}';
    }
}
