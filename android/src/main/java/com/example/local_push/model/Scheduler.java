package com.example.local_push.model;


public class Scheduler {
    int schedulerId;
    int userId;
    int boxId;
    String category;
    String title;
    String picture;
    boolean alarmUser;
    boolean notifySupervisor;
    Dattern dattern;
    String locale;

    @Override
    public String toString() {
        return "Scheduler{" +
                "schedulerId=" + schedulerId +
                ", userId=" + userId +
                ", boxId=" + boxId +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", picture='" + picture + '\'' +
                ", alarmUser=" + alarmUser +
                ", notifySupervisor=" + notifySupervisor +
                ", dattern=" + dattern +
                ", locale='" + locale + '\'' +
                '}';
    }

    public int getSchedulerId() {
        return schedulerId;
    }

    public void setSchedulerId(int schedulerId) {
        this.schedulerId = schedulerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isAlarmUser() {
        return alarmUser;
    }

    public void setAlarmUser(boolean alarmUser) {
        this.alarmUser = alarmUser;
    }

    public boolean isNotifySupervisor() {
        return notifySupervisor;
    }

    public void setNotifySupervisor(boolean notifySupervisor) {
        this.notifySupervisor = notifySupervisor;
    }

    public Dattern getDattern() {
        return dattern;
    }

    public void setDattern(Dattern dattern) {
        this.dattern = dattern;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
