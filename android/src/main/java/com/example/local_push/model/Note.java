package com.example.local_push.model;

public class Note {
    int noteId;
    String picture;
    String text;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Note{" +
                "noteId=" + noteId +
                ", picture='" + picture + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
