package com.example.local_push.model;

import java.util.List;

public class Box {
    int boxId;
    int userId;
    String uri;
    boolean invalid;
    String name;
    String location;
    String category;
    int brandId;
    int productId;
    List<Group> groups;
    List<Item> items;
    List<Note> notes;
    List<Calendar> calendars;
    List<Scheduler> schedulers;
    int maxItemCount;
    int maxNoteCount;
    String noteVoice;
    String noteText;
    String sealed;
    int sealedTime;
    int lifeYears;
    String isGift;
    UserBoxInfo userBoxInfo;

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<Calendar> getCalendars() {
        return calendars;
    }

    public void setCalendars(List<Calendar> calendars) {
        this.calendars = calendars;
    }

    public List<Scheduler> getSchedulers() {
        return schedulers;
    }

    public void setSchedulers(List<Scheduler> schedulers) {
        this.schedulers = schedulers;
    }

    public int getMaxItemCount() {
        return maxItemCount;
    }

    public void setMaxItemCount(int maxItemCount) {
        this.maxItemCount = maxItemCount;
    }

    public int getMaxNoteCount() {
        return maxNoteCount;
    }

    public void setMaxNoteCount(int maxNoteCount) {
        this.maxNoteCount = maxNoteCount;
    }

    public String getNoteVoice() {
        return noteVoice;
    }

    public void setNoteVoice(String noteVoice) {
        this.noteVoice = noteVoice;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public String getSealed() {
        return sealed;
    }

    public void setSealed(String sealed) {
        this.sealed = sealed;
    }

    public int getSealedTime() {
        return sealedTime;
    }

    public void setSealedTime(int sealedTime) {
        this.sealedTime = sealedTime;
    }

    public int getLifeYears() {
        return lifeYears;
    }

    public void setLifeYears(int lifeYears) {
        this.lifeYears = lifeYears;
    }

    public String getIsGift() {
        return isGift;
    }

    public void setIsGift(String isGift) {
        this.isGift = isGift;
    }

    public UserBoxInfo getUserBoxInfo() {
        return userBoxInfo;
    }

    public void setUserBoxInfo(UserBoxInfo userBoxInfo) {
        this.userBoxInfo = userBoxInfo;
    }

    @Override
    public String toString() {
        return "Box{" +
                "boxId=" + boxId +
                ", userId=" + userId +
                ", uri='" + uri + '\'' +
                ", invalid=" + invalid +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", category='" + category + '\'' +
                ", brandId=" + brandId +
                ", productId=" + productId +
                ", groups=" + groups +
                ", items=" + items +
                ", notes=" + notes +
                ", calendars=" + calendars +
                ", schedulers=" + schedulers +
                ", maxItemCount=" + maxItemCount +
                ", maxNoteCount=" + maxNoteCount +
                ", noteVoice='" + noteVoice + '\'' +
                ", noteText='" + noteText + '\'' +
                ", sealed='" + sealed + '\'' +
                ", sealedTime=" + sealedTime +
                ", lifeYears=" + lifeYears +
                ", isGift='" + isGift + '\'' +
                ", userBoxInfo=" + userBoxInfo +
                '}';
    }
}
