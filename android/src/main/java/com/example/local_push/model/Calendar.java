package com.example.local_push.model;

import java.util.List;

public class Calendar {
    int calendarId;
    int userId;
    String name;
    boolean enabled;
    List<Entry> entries;
    boolean supervisorNotify;

    public int getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(int calendarId) {
        this.calendarId = calendarId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public boolean isSupervisorNotify() {
        return supervisorNotify;
    }

    public void setSupervisorNotify(boolean supervisorNotify) {
        this.supervisorNotify = supervisorNotify;
    }

    @Override
    public String toString() {
        return "Calendar{" +
                "calendarId=" + calendarId +
                ", userId=" + userId +
                ", name='" + name + '\'' +
                ", enabled=" + enabled +
                ", entries=" + entries +
                ", supervisorNotify=" + supervisorNotify +
                '}';
    }
}
