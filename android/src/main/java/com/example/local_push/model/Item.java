package com.example.local_push.model;

public class Item {
    int itemId;
    int boxId;
    String boxUri;
    String name;
    boolean enableTip;
    String locale;
    int timezone;
    int tipTime;
    int storeTime;
    int warnTime;
    int startTime;
    int expireTime;
    String noteImage;
    String noteText;
    int tmpDays;
    boolean tmpNext;
    String tmpBoxName;
    String tmpBoxCategory;
    String scheme;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public String getBoxUri() {
        return boxUri;
    }

    public void setBoxUri(String boxUri) {
        this.boxUri = boxUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnableTip() {
        return enableTip;
    }

    public void setEnableTip(boolean enableTip) {
        this.enableTip = enableTip;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public int getTipTime() {
        return tipTime;
    }

    public void setTipTime(int tipTime) {
        this.tipTime = tipTime;
    }

    public int getStoreTime() {
        return storeTime;
    }

    public void setStoreTime(int storeTime) {
        this.storeTime = storeTime;
    }

    public int getWarnTime() {
        return warnTime;
    }

    public void setWarnTime(int warnTime) {
        this.warnTime = warnTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        this.expireTime = expireTime;
    }

    public String getNoteImage() {
        return noteImage;
    }

    public void setNoteImage(String noteImage) {
        this.noteImage = noteImage;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public int getTmpDays() {
        return tmpDays;
    }

    public void setTmpDays(int tmpDays) {
        this.tmpDays = tmpDays;
    }

    public boolean isTmpNext() {
        return tmpNext;
    }

    public void setTmpNext(boolean tmpNext) {
        this.tmpNext = tmpNext;
    }

    public String getTmpBoxName() {
        return tmpBoxName;
    }

    public void setTmpBoxName(String tmpBoxName) {
        this.tmpBoxName = tmpBoxName;
    }

    public String getTmpBoxCategory() {
        return tmpBoxCategory;
    }

    public void setTmpBoxCategory(String tmpBoxCategory) {
        this.tmpBoxCategory = tmpBoxCategory;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", boxId=" + boxId +
                ", boxUri='" + boxUri + '\'' +
                ", name='" + name + '\'' +
                ", enableTip=" + enableTip +
                ", locale='" + locale + '\'' +
                ", timezone=" + timezone +
                ", tipTime=" + tipTime +
                ", storeTime=" + storeTime +
                ", warnTime=" + warnTime +
                ", startTime=" + startTime +
                ", expireTime=" + expireTime +
                ", noteImage='" + noteImage + '\'' +
                ", noteText='" + noteText + '\'' +
                ", tmpDays=" + tmpDays +
                ", tmpNext=" + tmpNext +
                ", tmpBoxName='" + tmpBoxName + '\'' +
                ", tmpBoxCategory='" + tmpBoxCategory + '\'' +
                ", scheme='" + scheme + '\'' +
                '}';
    }
}
