package com.example.local_push.model;


import java.io.Serializable;

public class Task implements Serializable {
    int taskId;
    int boxId;
    int calendarId;
    int entryId;
    int itemId;
    int schedulerId;
    int closed;
    String uri;
    String title;
    String content;
    int dueTime;
    int taskDate;
    int taskTime;
    Data data;
    int schedulerTaskTotal;
    int schedulerTaskDone;
    public Task(){}
    public Task(int taskId, int boxId, int calendarId, int entryId, int itemId, int schedulerId, int closed, String uri, String title, String content, int dueTime, int taskDate, int taskTime, Data data, int schedulerTaskTotal, int schedulerTaskDone) {
        this.taskId = taskId;
        this.boxId = boxId;
        this.calendarId = calendarId;
        this.entryId = entryId;
        this.itemId = itemId;
        this.schedulerId = schedulerId;
        this.closed = closed;
        this.uri = uri;
        this.title = title;
        this.content = content;
        this.dueTime = dueTime;
        this.taskDate = taskDate;
        this.taskTime = taskTime;
        this.data = data;
        this.schedulerTaskTotal = schedulerTaskTotal;
        this.schedulerTaskDone = schedulerTaskDone;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public int getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(int calendarId) {
        this.calendarId = calendarId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getSchedulerId() {
        return schedulerId;
    }

    public void setSchedulerId(int schedulerId) {
        this.schedulerId = schedulerId;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getDueTime() {
        return dueTime;
    }

    public void setDueTime(int dueTime) {
        this.dueTime = dueTime;
    }

    public int getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(int taskDate) {
        this.taskDate = taskDate;
    }

    public int getTaskTime() {
        return taskTime;
    }

    public void setTaskTime(int taskTime) {
        this.taskTime = taskTime;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getSchedulerTaskTotal() {
        return schedulerTaskTotal;
    }

    public void setSchedulerTaskTotal(int schedulerTaskTotal) {
        this.schedulerTaskTotal = schedulerTaskTotal;
    }

    public int getSchedulerTaskDone() {
        return schedulerTaskDone;
    }

    public void setSchedulerTaskDone(int schedulerTaskDone) {
        this.schedulerTaskDone = schedulerTaskDone;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", boxId=" + boxId +
                ", calendarId=" + calendarId +
                ", entryId=" + entryId +
                ", itemId=" + itemId +
                ", schedulerId=" + schedulerId +
                ", closed=" + closed +
                ", uri='" + uri + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", dueTime=" + dueTime +
                ", taskDate=" + taskDate +
                ", taskTime=" + taskTime +
                ", data=" + data +
                ", schedulerTaskTotal=" + schedulerTaskTotal +
                ", schedulerTaskDone=" + schedulerTaskDone +
                '}';
    }
}

