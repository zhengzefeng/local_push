package com.example.local_push.model;

public class Entry {
    int calendarId;
    int entryId;
    boolean enabled;
    int time;
    String scheduler;
    String text;
    Data data;
    int everyDays;
    int everyDaysOffset;
    String locale;

    public int getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(int calendarId) {
        this.calendarId = calendarId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getScheduler() {
        return scheduler;
    }

    public void setScheduler(String scheduler) {
        this.scheduler = scheduler;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getEveryDays() {
        return everyDays;
    }

    public void setEveryDays(int everyDays) {
        this.everyDays = everyDays;
    }

    public int getEveryDaysOffset() {
        return everyDaysOffset;
    }

    public void setEveryDaysOffset(int everyDaysOffset) {
        this.everyDaysOffset = everyDaysOffset;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "calendarId=" + calendarId +
                ", entryId=" + entryId +
                ", enabled=" + enabled +
                ", time=" + time +
                ", scheduler='" + scheduler + '\'' +
                ", text='" + text + '\'' +
                ", data=" + data +
                ", everyDays=" + everyDays +
                ", everyDaysOffset=" + everyDaysOffset +
                ", locale='" + locale + '\'' +
                '}';
    }
}
