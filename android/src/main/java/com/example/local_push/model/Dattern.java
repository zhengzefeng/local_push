package com.example.local_push.model;

import java.util.List;


public class Dattern {
    int startDate;
    int endDate;
    String state;
    String scheme;
    List<Boolean> weekDays;
    int periodDays;
    int durationDays;
    List<Integer> timers;

    public int getStartDate() {
        return startDate;
    }

    public void setStartDate(int startDate) {
        this.startDate = startDate;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public List<Boolean> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<Boolean> weekDays) {
        this.weekDays = weekDays;
    }

    public int getPeriodDays() {
        return periodDays;
    }

    public void setPeriodDays(int periodDays) {
        this.periodDays = periodDays;
    }

    public int getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(int durationDays) {
        this.durationDays = durationDays;
    }

    public List<Integer> getTimers() {
        return timers;
    }

    public void setTimers(List<Integer> timers) {
        this.timers = timers;
    }

    @Override
    public String toString() {
        return "Dattern{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", state='" + state + '\'' +
                ", scheme='" + scheme + '\'' +
                ", weekDays=" + weekDays +
                ", periodDays=" + periodDays +
                ", durationDays=" + durationDays +
                ", timers=" + timers +
                '}';
    }
}
