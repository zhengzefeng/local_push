package com.example.local_push.model;

public class UserBoxInfo {
    int userId;
    String category;
    int serviceLife;
    int boxCount;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(int serviceLife) {
        this.serviceLife = serviceLife;
    }

    public int getBoxCount() {
        return boxCount;
    }

    public void setBoxCount(int boxCount) {
        this.boxCount = boxCount;
    }

    @Override
    public String toString() {
        return "UserBoxInfo{" +
                "userId=" + userId +
                ", category='" + category + '\'' +
                ", serviceLife=" + serviceLife +
                ", boxCount=" + boxCount +
                '}';
    }
}
