package com.example.local_push.worker;

import android.content.Context;

import com.example.local_push.HandlerProgress;
import com.example.local_push.data.DataStore;
import com.example.local_push.service.PushService;
import com.example.local_push.util.DateUtil;
import com.example.local_push.util.LogUtil;
import com.example.local_push.util.ServiceUtil;

import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class AlarmWorker extends Worker {
    Context context;

    public AlarmWorker(@NonNull Context context0, @NonNull WorkerParameters workerParams) {
        super(context0, workerParams);
        context = context0;
        DataStore.getInstance().init(context);
    }

//    @NonNull
//    @Override
//    public Result doWork() {
//
//        long time = DataStore.getInstance().getLastNotificationtime();
//
////        int minter = DateUtil.getTimeByCalendar(Calendar.getInstance());
//        long minuter =DateUtil.getMinuterByTime(Calendar.getInstance().getTimeInMillis()) ;
//        LogUtil.print("doWork minuter:" + minuter + ",older:" + time);
//        if (minuter <= time)
//            return Worker.Result.success();
//        LogUtil.print("doWork run startServer");
//
//        ServiceUtil.startService(context, PushService.class);
//        return Worker.Result.success();
//
//    }

    @NonNull
    @Override
    public Result doWork() {

        LogUtil.print("****************************doWork!!!!!!!!!!!!****************************");
//        startLoop();
        HandlerProgress.run(context);
//        long time = DataStore.getInstance().getLastNotificationtime();
//
////        int minter = DateUtil.getTimeByCalendar(Calendar.getInstance());
//        long minuter =DateUtil.getMinuterByTime(Calendar.getInstance().getTimeInMillis()) ;
//        LogUtil.print("doWork minuter:" + minuter + ",older:" + time);
//        if (minuter <= time)
//            return Worker.Result.success();
//        LogUtil.print("doWork run startServer");
//
//        ServiceUtil.startService(context, PushService.class);

        return Worker.Result.success();

    }

//    private boolean isRunLoop = false;
//    private HandlerProgress progress;

//    public void startLoop() {
//        LogUtil.print("isRunLoop:" + isRunLoop);
//        if (progress != null) {
//            stopLoop();
//        }
//        progress = new HandlerProgress();
//        progress.startLoop(context);
//    }
//
//
//    public void stopLoop() {
//        LogUtil.print("startLoop:");
//        progress.stop();
//    }
}
