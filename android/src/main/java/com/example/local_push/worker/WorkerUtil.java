package com.example.local_push.worker;

import android.content.Context;

import com.example.local_push.data.DataStore;
import com.example.local_push.util.DateUtil;
import com.example.local_push.util.LogUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.Worker;

public class WorkerUtil {
    public static void run() {
        WorkManager.getInstance().cancelAllWork();
        WorkManager.getInstance().beginWith(request(10)).then(request(10)).then(request(10)).then(request(10)).enqueue();
    }

    public static OneTimeWorkRequest request(int seconds) {
        return new OneTimeWorkRequest.Builder(AlarmWorker.class)
//                .setInitialDelay(seconds, TimeUnit.SECONDS)
//                .build();
                .setInitialDelay(seconds, TimeUnit.SECONDS).build();

    }

    public static void close(Context context) {
        WorkManager.getInstance(context).cancelAllWork();
    }

    public static void start(Context context) {
        LogUtil.print("WorkerUtil start");
        DataStore.getInstance().init(context);
        WorkManager.getInstance(context).enqueue(new OneTimeWorkRequest.Builder(AlarmWorker.class).setInitialDelay(1, TimeUnit.SECONDS).build());
    }

    public static void setTimes(Context context, List<Calendar> calendarList) {
        if (calendarList.size() == 0) {
            return;
        }
        WorkManager.getInstance(context).cancelAllWork();
//        WorkManager.getInstance(context).ca();
        long nowTime = Calendar.getInstance().getTimeInMillis();
        List<WorkRequest> workRequests = new ArrayList<>();
        for (int i = 0; i < calendarList.size(); i++) {
            long calendarTime = calendarList.get(i).getTimeInMillis();
            if (DateUtil.getMinuterByTime(calendarTime) == DateUtil.getMinuterByTime(Calendar.getInstance().getTimeInMillis())) {
                continue;
            }
            long delayTime = calendarTime - nowTime;
            WorkRequest workRequest = new OneTimeWorkRequest.Builder(AlarmWorker.class).setInitialDelay(delayTime, TimeUnit.MILLISECONDS).build();
            workRequests.add(workRequest);
        }
        WorkManager.getInstance(context).enqueue(workRequests);
    }

    public static void testRun(Context context) {
        List<Calendar> calendarList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);
        calendarList.add(calendar);
        WorkManager.getInstance(context).cancelAllWork();
        long nowTime = Calendar.getInstance().getTimeInMillis();
        List<WorkRequest> workRequests = new ArrayList<>();
        Constraints constraints = new Constraints.Builder()
//                .setRequiresBatteryNotLow(false)//不在电量不足时执行
//                .setRequiresCharging(false)//在充电时执行
//                .setRequiresStorageNotLow(false)//不在存储容量不足时执行
//                .setRequiresDeviceIdle(false)
                //在待机状态执行
                .build();
        for (int i = 0; i < calendarList.size(); i++) {
            long calendarTime = calendarList.get(i).getTimeInMillis();
            long delayTime = calendarTime - nowTime;
            if (DateUtil.getMinuterByTime(calendarTime) <= DateUtil.getMinuterByTime(nowTime)) {
                continue;
            }
            LogUtil.print("xxxxx delayTime:" + delayTime);
            WorkRequest workRequest = new OneTimeWorkRequest.Builder(AlarmWorker.class).setConstraints(constraints).setInitialDelay(delayTime, TimeUnit.MILLISECONDS).build();
            workRequests.add(workRequest);
        }
        if (workRequests.size() > 0)
            WorkManager.getInstance(context).enqueue(workRequests);
    }
}