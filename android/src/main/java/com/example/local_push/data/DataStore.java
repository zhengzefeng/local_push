package com.example.local_push.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.example.local_push.model.Box;
import com.example.local_push.model.Scheduler;
import com.example.local_push.model.Task;

import java.util.ArrayList;
import java.util.List;

public class DataStore {
    static DataStore dataStore;
    static final String spName = "dataStore";
    static final String sp_shedulers = "sp_shedulers";
    static final String sp_freshtasks = "sp_freshtasks";

    static final String sp_boxes = "sp_boxes";

    static final String sp_fresh_tip = "sp_fresh_tip";
    static final String sp_tasks = "sp_tasks";
    static final String sp_alarm_max_code = "sp_alarm_max_code";
    static final String sp_last_notificationtime = "sp_last_notificationtime_3";

    SharedPreferences sharedPreferences;

    public static DataStore getInstance() {
        if (dataStore == null) {
            dataStore = new DataStore();
        }
        return dataStore;
    }

    public void init(Context context) {
        sharedPreferences = context.getSharedPreferences(spName, Context.MODE_PRIVATE);
    }

    public void setSchedulers(String objStr) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sp_shedulers, objStr);
        editor.apply();
    }

    public List<Scheduler> getSchedulers() {
        return JSON.parseObject(sharedPreferences.getString(sp_shedulers, "[]"), new TypeReference<List<Scheduler>>() {
        });
    }

    public void setTasks(String objStr) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sp_tasks, objStr);
        editor.apply();
    }

    public List<Task> getTasks() {
        return JSON.parseObject(sharedPreferences.getString(sp_tasks, "[]"), new TypeReference<List<Task>>() {
        });
    }

    public void setAlarmMaxCode(int alarm_code) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(sp_alarm_max_code, alarm_code);
        editor.apply();
    }

    public int getAlarmCodes() {
        return sharedPreferences.getInt(sp_alarm_max_code, 0);
    }

    public int addAlarmMaxCode() {
        int code = getAlarmCodes() + 1;
        setAlarmMaxCode(code);
        return code;
    }

    public void setLastNotificationtime(long days) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(sp_last_notificationtime, days);
        editor.apply();
    }

    public long getLastNotificationtime() {
        return sharedPreferences.getLong(sp_last_notificationtime, 0);
    }

    //    public void setLastNotificationtime(int days, long minuter) {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString(sp_last_notificationtime, (days + "," + minuter));
//        editor.apply();
//    }
//
//    public List<Integer> getLastNotificationtime() {
//        String arrStr = sharedPreferences.getString(sp_last_notificationtime, "");
//        List<Integer> arrayList = new ArrayList<>();
//        if (arrStr == "") {
//            arrayList.add(0);
//            arrayList.add(0);
//        } else {
//            String[] arr = arrStr.split(",");
//            arrayList.add(Integer.parseInt(arr[0]));
//            arrayList.add(Integer.parseInt(arr[1]));
//        }
//        return arrayList;
//    }
    public void setBoxes(String objStr) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sp_boxes, objStr);
        editor.apply();
    }

    public List<Box> getBoxes() {
        return JSON.parseObject(sharedPreferences.getString(sp_boxes, "[]"), new TypeReference<List<Box>>() {
        });
    }

    public void setFreshTasks(String objStr) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sp_freshtasks, objStr);
        editor.apply();
    }

    public List<Task> getFreshTasks() {
        return JSON.parseObject(sharedPreferences.getString(sp_freshtasks, "[]"), new TypeReference<List<Task>>() {
        });
    }

}
