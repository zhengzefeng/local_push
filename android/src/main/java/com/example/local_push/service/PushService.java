package com.example.local_push.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.example.local_push.HandlerProgress;
import com.example.local_push.data.DataStore;
import com.example.local_push.util.NotificationChannels;


public class PushService extends Service {
    final String TAG = "tag";
    private LocalBinder mbinder = new LocalBinder();

    public PushService() {
    }

    public class LocalBinder extends Binder {
        public PushService getservices() {
            return PushService.this;
        }

        public void start() {
            Log.e(TAG, "start:");
        }

        public void end() {
            Log.e(TAG, "end:");
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind:");
        return mbinder;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate:");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        DataStore.getInstance().init(this);
        Log.e(TAG, "onStartCommand:");
        startLoop();
//        startForeground(1, NotificationChannels.createNotificationChannel(this));
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy:");
        stopLoop();
        super.onDestroy();
    }

    public String myway() {
        Log.e(TAG, "myway:hello world");
        return "hello world";
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(TAG, "onUnbind:");
        return super.onUnbind(intent);
    }

    int index = 0;
    private boolean isRunLoop = false;
    private HandlerProgress progress;

    public void startLoop() {
        Log.e(TAG, "isRunLoop:" + isRunLoop);
        if (progress != null) {
            stopLoop();
        }
        progress = new HandlerProgress();
        progress.startLoop(this);
    }


    public void stopLoop() {
        Log.e(TAG, "startLoop:");
        progress.stop();
    }

}

