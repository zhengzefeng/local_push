package com.example.local_push.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.local_push.HandlerProgress;
import com.example.local_push.data.DataStore;
import com.example.local_push.util.LogUtil;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

public class MyIntentJobService extends JobIntentService {

    private static final int JOB_ID = 10002;

    public static void startService(Context context, Intent work) {
        enqueueWork(context, MyIntentJobService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // 处理具体的逻辑
        DataStore.getInstance().init(this);
        startLoop();
    }

    @Override
    public boolean onStopCurrentWork() {
        LogUtil.print("onStopCurrentWork");
        return super.onStopCurrentWork();
    }

    private boolean isRunLoop = false;
    private HandlerProgress progress;

    public void startLoop() {
        LogUtil.print("MyIntentJobService isRunLoop:" + isRunLoop);
        if (progress != null) {
            stopLoop();
        }
        progress = new HandlerProgress();
        progress.startLoop(this);
    }


    public void stopLoop() {
        LogUtil.print("startLoop:");
        progress.stop();
    }
}
