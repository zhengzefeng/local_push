package com.example.local_push.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.Message;

import com.example.local_push.util.LogUtil;
import com.example.local_push.util.NotificationChannels;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobAlarmService extends JobService {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        //TODO 写上你的任务逻辑代码
        LogUtil.print("JobAlarmService onStartJob");
        NotificationChannels.show(this, "JobAlarmService", "JobAlarmService 启动完成！！！！！！！！！！");
        jobFinished(params, false);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
