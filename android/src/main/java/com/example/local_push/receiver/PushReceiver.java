package com.example.local_push.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.local_push.service.PushService;
import com.example.local_push.util.AlarmUtil;
import com.example.local_push.util.LogUtil;
import com.example.local_push.util.NotificationChannels;
import com.example.local_push.util.ServiceUtil;

public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationChannels.show(context,"PushReceiver","测试PushReceiver的接收");
      LogUtil.print ("************************************************************ receiver ******************************************");
        if (intent.getAction().equals(AlarmUtil.action)) {
//            if(!ServiceUtil.isServiceRunning(context,PushService.class.getName())){
                ServiceUtil.startService(context, PushService.class);
//            }
        }
    }
}
