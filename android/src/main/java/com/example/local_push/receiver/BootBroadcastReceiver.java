package com.example.local_push.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.local_push.util.LogUtil;
import com.example.local_push.util.NotificationChannels;
import com.example.local_push.worker.WorkerUtil;

public class BootBroadcastReceiver extends BroadcastReceiver {
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            LogUtil.print("*******************reboot!!!!!!!!!!!!!!*******************");
//            NotificationChannels.show(context, "重启标题", "内容");
            WorkerUtil.start(context);
        }
    }

}
