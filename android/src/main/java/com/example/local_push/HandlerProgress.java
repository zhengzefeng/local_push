package com.example.local_push;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.example.local_push.data.DataStore;
import com.example.local_push.model.Box;
import com.example.local_push.model.Dattern;
import com.example.local_push.model.Scheduler;
import com.example.local_push.model.Task;
import com.example.local_push.util.AlarmUtil;
import com.example.local_push.util.CloneObjectUtils;
import com.example.local_push.util.DateUtil;
import com.example.local_push.util.LogUtil;
import com.example.local_push.util.NotificationChannels;
import com.example.local_push.util.TaskUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HandlerProgress {
    //    Date date;
//    List<Date> dates = new ArrayList<Date>();
    Thread loopThread;
    //    int index = 0;
    List<Scheduler> schedulers;
    List<Task> freshTasks;

    List<Task> specialTodayTasks; //20分钟前 30分钟后区间的tasks
    List<Box> boxes; //20分钟前 30分钟后区间的tasks

    boolean isLoop = true;
//    int cacheIndex = 0;
//    int timeStamp = -1;

    public HandlerProgress() {
//        Date now = Calendar.getInstance().getTime();
//        Date date = new Date();
//        date.setTime(now.getTime() + 1000 * 3);
//        dates.add(date);
//        date = new Date();
//        date.setTime(now.getTime() + 1000 * 6);
//        dates.add(date);
//        date = new Date();
//        date.setTime(now.getTime() + 1000 * 9);
//        dates.add(date);
//        date = new Date();
//        date.setTime(now.getTime() + 1000 * 12);
//        dates.add(date);
//        date = new Date();
//        date.setTime(now.getTime() + 1000 * 16);
//        dates.add(date);
        initData();
    }

    public static HandlerProgress _handlerProgress;


    public static void run(final Context context) {
        if (_handlerProgress == null) {
            _handlerProgress = new HandlerProgress();
        } else {
            _handlerProgress.stop();
        }
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                _handlerProgress = new HandlerProgress();
                _handlerProgress.startLoop(context);
            }
        }.start();
    }


//    public void startLoop(final Context context) {
//        startLoop(context);
//    }

    public void startLoop(final Context context) {
        LogUtil.print("start loop*******************************");
        //要处理多个计划同时出现的处理,现在这样做  最多一分钟只能显示6个
        //计算相同的时间任务个数，再根据这个值进行处理
        //多个计划 同一个时间 其中一个点击完成之后，会重置所有计划， 要把今日任务传过来,把已经完成的过滤，再显示
        //新思路，创建当天的任务列表(半个小时前到今天结束)（以及超出今天的可能存在的重复提示task）,当检测没计划的时候，
        //计算现在到第二天的时间,延时等待。 时间戳保存已经推出到信息， 第二天的任务, 超出今天新建一个线程 去处理  前半个小时内的，也要单独开线程
        loopThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int calculationDays = 0;
                List<Task> calculationTasks = new ArrayList<>();
                isLoop = true;
                while (!Thread.interrupted()) {
                    try {
                        LogUtil.print("loop first***********************************************************************");
                        int nowDays = (int) DateUtil.getDaysByCalendar(Calendar.getInstance());
                        if (nowDays != calculationDays) {
                            calculationDays = nowDays;
                            calculationTasks = getTodayTasks();
                        }
                        AlarmUtil.setTimes(context, getLastSchedulerCalendars());//计算6个任务时间
                        if (calculationTasks.size() == 0) {
                            int startTime = DateUtil.getTimeByCalendar(Calendar.getInstance());
                            int waitTime = (24 * 60 - startTime) * 1000;
                            LogUtil.print("todayTask is finished,  wait time:" + " ,delayYime:" + waitTime / 1000 / 60);
                            Thread.sleep(waitTime);
                            return;
                        }

                        Task task = calculationTasks.get(0);
                        long targetTime = TaskUtil.getTimeInMillisByTask(task);
                        long nowTime = Calendar.getInstance().getTimeInMillis();
                        long delayTime = targetTime - nowTime;
                        if (delayTime < 0) {
                            delayTime = 0;
                        }

                        //时间短暂化
                        if (delayTime / 1000 / 60 > 5) { //大于5分钟 睡一半时间醒过来 重新计算
                            LogUtil.print("sleep some time");
                            Thread.sleep(delayTime / 2);
                            return;
                        }

                        LogUtil.print("wait task:" + task.toString() + ",targetTime:" + task.getTaskTime() / 60 + ":" + task.getTaskTime() % 60 + " ,delayYime:" + delayTime + "--" + (delayTime / 1000 / 60));
                        Thread.sleep(delayTime);
                        LogUtil.print("线程睡眠时间到了 isLoop:" + Thread.interrupted());
                        if (Thread.interrupted()) {
                            return;
                        }
                        long time = DataStore.getInstance().getLastNotificationtime();
                        long minuter = DateUtil.getMinuterByTime(targetTime);
                        if (minuter <= time) {
                            calculationTasks.remove(0);
                            return;
                        }
                        DataStore.getInstance().setLastNotificationtime(minuter);
                        calculationTasks = handleTasks(context, calculationTasks, targetTime);
                        updateData();
                    } catch (InterruptedException e) {
                        LogUtil.print("线程退出：" + e.toString());
                        break;
                    }
                }

            }
        });
        try {
            loopThread.start();
        } catch (
                Exception e) {
            LogUtil.print(e.toString());
        }

    }

    public void stop() {
        loopThread.interrupt();
        LogUtil.print("stop handle progress");
//        dates = new ArrayList<Date>();
    }

    private void setAlarms() {

    }

    void initData() {
        schedulers = DataStore.getInstance().getSchedulers();
        specialTodayTasks = DataStore.getInstance().getTasks();
        boxes = DataStore.getInstance().getBoxes();
        freshTasks = DataStore.getInstance().getFreshTasks();
        LogUtil.print("initData:" + schedulers.toString());
        LogUtil.print("specialTodayTasks:" + specialTodayTasks.toString());

    }

    public void updateData() {
        String result = JSON.toJSONString(schedulers);
        DataStore.getInstance().setSchedulers(result);
    }

    private List<Task> getTodayTasks() {
        int nowDays = (int) DateUtil.getDaysByCalendar(Calendar.getInstance());
        int nowTime = DateUtil.getTimeByCalendar(Calendar.getInstance());
        List<Task> tasks = getTasksByDateTime(nowDays, nowTime);
        List<Task> resultTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getTaskTime() >= nowTime) {
                resultTasks.add(task);
            }
        }
        return resultTasks;
    }

    //取不同时间最近的6个任务的时间
    private List<Calendar> getLastSchedulerCalendars() {
        List<Calendar> calendars = new ArrayList<>();
        List<Task> tasks = getLast6Task();
        LogUtil.print("getLast6Task:" + tasks.toString());
        for (Task task : tasks) {
            Calendar dateCalendar = DateUtil.getCalendarByDays(task.getTaskDate());
            int year = dateCalendar.get(Calendar.YEAR);
            int month = dateCalendar.get(Calendar.MONTH);
            int dayOfMonth = dateCalendar.get(Calendar.DAY_OF_MONTH);
            dateCalendar.set(year, month, dayOfMonth, task.getTaskTime() / 60, task.getTaskTime() % 60);
            calendars.add(dateCalendar);
        }
        if (tasks.size() > 2) {
            if (tasks.get(0).getTaskDate() == tasks.get(tasks.size() - 1).getTaskDate()) {//最近6个闹钟都在今天的时候，记另外3个其他天数
                Calendar dateCalendar = DateUtil.getCalendarByDays(tasks.get(0).getTaskDate());
                dateCalendar.add(Calendar.DAY_OF_MONTH, 1);
                calendars.add(dateCalendar);
                Calendar dateCalendar1 = DateUtil.getCalendarByDays(tasks.get(0).getTaskDate());
                dateCalendar1.add(Calendar.DAY_OF_MONTH, 2);
                calendars.add(dateCalendar1);
                Calendar dateCalendar2 = DateUtil.getCalendarByDays(tasks.get(0).getTaskDate());
                dateCalendar2.add(Calendar.DAY_OF_MONTH, 3);
                calendars.add(dateCalendar2);
            }

        }
        List<String> testStr = new ArrayList<>();
        for (int i = 0; i < calendars.size(); i++) {
            String pattern = "yyyy-MM-dd HH:mm";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(calendars.get(i).getTime());
            testStr.add(date);
        }
        LogUtil.print("last task date:" + testStr.toString());
        return calendars;
    }

    //TODO:可能没有6个，极限情况下 获取不同时间最近的6个任务
    private List<Task> getLast6Task() {
        int i = 0;
        int days = 0;
        int lastCount = 6;
        List<Task> tasks = new ArrayList<>();
        while (days < 180 && tasks.size() < lastCount) {
            int selectDays = (int) DateUtil.getDaysByCalendar(Calendar.getInstance());
            List<Task> selectTasks = new ArrayList<>();
            if (days == 0) {
                selectTasks = getTodayTasks();
            } else {
                selectTasks = getTasksByDateTime(selectDays + days, 0);
            }
            List<Task> tempTasks = new ArrayList<>();

            for (Task task : selectTasks) {
                if (tempTasks.size() == 0) {
                    tempTasks.add(task);
                } else {
                    Task tempLastTask = tempTasks.get(tempTasks.size() - 1);
                    if (TaskUtil.getTimeInMillisByTask(tempLastTask) < TaskUtil.getTimeInMillisByTask(task)) {
                        tempTasks.add(task);
                    }
                }
            }
            selectTasks = tempTasks;
//            List<Task> selectTasks =
            int canAddCount = lastCount - tasks.size();
            if (canAddCount >= selectTasks.size())
                tasks.addAll(selectTasks);
            else {
                tasks.addAll(selectTasks.subList(0, canAddCount));
            }
            days++;
        }
        return tasks;
    }


    Task handleTask(Scheduler scheduler, int nowDays, int nowTime, int closed) {
        Task task = new Task();
//        task.setContent("这是计划来自<< " + scheduler.getTitle() + " >>");
        task.setSchedulerId(scheduler.getSchedulerId());
        task.setTaskDate(nowDays);
        task.setTaskTime(nowTime);
        task.setClosed(closed);
        task.setTitle("");
//        task.setContent(TaskUtil.getTimeByTask(task) + " 服用时间到了！请在服用后，打开KIGI完成计划.（只有中文翻译)");
        for (Box box : boxes) {
            if (box.getBoxId() == scheduler.getBoxId()) {
                task.setTitle(box.getName());
            }
        }
        if (task.getTitle().equals("")) {
            task.setTitle("盒子未知,请联系客服");
        }
        return task;
    }


    Task handleTask(Task freshTask, int nowDays, int nowTime) {

//        freshTask.setTitle("");
//        freshTask.setContent(TaskUtil.getTimeByTask(task) + " 服用时间到了！请在服用后，打开KIGI完成计划.（只有中文翻译)");
//
//        if (freshTask.getTitle().equals("")) {
//            freshTask.setTitle("盒子未知,请联系客服");
//        }
        return freshTask;
    }

    public List<Task> getTasksByDateTime(int nowDays, int nowTime) {
        if (schedulers.size() == 0) {
            return new ArrayList<>();
        }
        int startTime = nowTime - 20;
        if (startTime < 0) {
            startTime = 0;
        }
        List<Task> allTasks = new ArrayList<>();
//        List<Task> specialTasks = new ArrayList<>();

        for (int i = 0; i < schedulers.size(); i++) {
            List<Task> tasks = new ArrayList<>();
            Scheduler scheduler = schedulers.get(i);
            Dattern dattern = scheduler.getDattern();
            int startDays = dattern.getStartDate();
            int periodDays = dattern.getPeriodDays();
            int endDays = dattern.getEndDate();
            if (endDays == 0) {
                endDays = (int) DateUtil.getDaysByCalendar(Calendar.getInstance()) + 365 * 10;
            }
            Collections.sort(dattern.getTimers());

            if (endDays < nowDays) {
                continue;
            }

            if (dattern.getState().equals("PillPlanState.regularPlan")) {
                if ((nowDays - startDays) % periodDays == 0) {
                    for (int time : scheduler.getDattern().getTimers()) {
                        if (time >= startTime) {
                            Task task = handleTask(scheduler, nowDays, time, 0);
                            tasks.add(task);
                        }
                    }
                }
            }
            if (dattern.getState().equals("PillPlanState.customScheme")) {
                if (dattern.getScheme().equals("CustomPlanState.jianGeShiJian")) {
                    if ((nowDays - startDays) % periodDays == 0) {
                        for (int time : scheduler.getDattern().getTimers()) {
                            if (time >= startTime) {
                                Task task = handleTask(scheduler, nowDays, time, 0);
                                tasks.add(task);
                            }
                        }
                    }
                }
                if (dattern.getScheme().equals("CustomPlanState.teDingRiQi")) {
                    Calendar calendar = DateUtil.getCalendarByDays(nowDays);
                    int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
//                        if (day_of_week == 7) {
//                            day_of_week = 0;
//                        }
                    if (dattern.getWeekDays().get(day_of_week - 1)) {
                        for (int time : scheduler.getDattern().getTimers()) {
                            if (time >= startTime) {
                                Task task = handleTask(scheduler, nowDays, time, 0);
                                tasks.add(task);
                            }
                        }
                    }

                }
                if (dattern.getScheme().equals("CustomPlanState.teDingZhouQi")) {
                    int days = nowDays - dattern.getStartDate() + 1;
                    int t = days % (dattern.getPeriodDays());
                    if ((t <= dattern.getDurationDays()) && t > 0) {
                        for (int time : scheduler.getDattern().getTimers()) {
                            if (time >= startTime) {
                                Task task = handleTask(scheduler, nowDays, time, 0);
                                tasks.add(task);
                            }
                        }
                    }
                }
            }
            //特殊任务 特别处理
            if (specialTodayTasks.size() > 0) {
                LogUtil.print("" +
                        "" + specialTodayTasks.toString());
                if (specialTodayTasks.get(0).getTaskDate() == nowDays) {
                    for (Task specialTask : specialTodayTasks) {
                        for (Task task0 : tasks) {
                            if (task0.getSchedulerId() == specialTask.getSchedulerId()) {
                                if (task0.getTaskTime() == specialTask.getTaskTime()) {
                                    LogUtil.print("特殊处理 task0: " + specialTask.toString());
                                    task0.setClosed(specialTask.getClosed());
                                }
                            }
                        }
                    }
                }
            }
            for (int k = 0; k < tasks.size(); k++) {
                Task task = tasks.get(k);
                if (task.getClosed() == 0) {
                    allTasks.add(task);
                }
            }
        }

        for (Task task : freshTasks) {
            if (task.getTaskDate() != nowDays) {
                continue;
            }
            if (specialTodayTasks.size() > 0) {
                if (specialTodayTasks.get(0).getTaskDate() == nowDays) {
                    for (Task specialTask : specialTodayTasks) {
                        if (task.getBoxId() == specialTask.getBoxId()) {
                            if (task.getTaskTime() == specialTask.getTaskTime()) {
                                LogUtil.print("特殊处理 task0: " + specialTask.toString());
                                task.setClosed(specialTask.getClosed());
                            }
                        }
                    }
                }
            }
            allTasks.add(task);
        }


//        LogUtil.print("allTask: (no complete)" + allTasks.toString());
        List<Task> completeTasks = new ArrayList<>();
        for (int i = 0; i < allTasks.size(); i++) {
            Task task = allTasks.get(i);
            Task task0 = CloneObjectUtils.cloneObject(task);
            if (task0.getTaskTime() < 23 * 60 + 30) {
                task0.setTaskTime(task0.getTaskTime() + 10);
            }
            Task task1 = CloneObjectUtils.cloneObject(task);
            if (task1.getTaskTime() < 23 * 60 + 30) {
                task1.setTaskTime(task1.getTaskTime() + 20);
            }
            completeTasks.add(task0);
            completeTasks.add(task1);
        }
        allTasks.addAll(completeTasks);


        Collections.sort(allTasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                if (o1.getTaskDate() != o2.getTaskDate()) {
                    return o1.getTaskDate() - o2.getTaskDate();
                } else {
                    return o1.getTaskTime() - o2.getTaskTime();
                }

            }
        });
        return allTasks;
    }

    List<Task> handleTasks(Context context, List<Task> tasks, long targetTime) {
        LogUtil.print("handleTasks before:" + tasks.toString());
        List<Task> tempTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (TaskUtil.getTimeInMillisByTask(task) == targetTime) {
                LogUtil.print("show notification!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                NotificationChannels.show(context, task.getTitle(), TaskUtil.getTaskContent(task));
            } else {
                tempTasks.add(task);
            }
        }
        LogUtil.print("handleTasks after");

        return tempTasks;

    }
}

