package com.example.local_push.util;

import com.example.local_push.model.Task;

import java.util.Calendar;

public class TaskUtil {
    public static long getTaskNotificationTime(Task task) {
        return task.getTaskTime() + task.getDueTime();
    }

    public static long getTimeInMillisByTask(Task task) {
        return DateUtil.getCalendarByDays(task.getTaskDate()).getTimeInMillis() + task.getTaskTime() * 1000 * 60;
    }

    public static String getTimeByTask(Task task) {
        int hour = task.getTaskTime() / 60;
        int minuter = task.getTaskTime() % 60;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minuter);
        return DateUtil.getDateFormatStr(calendar, "HH:mm");
    }

    public static String getTaskContent(Task task) {
        if (task.getSchedulerId() != 0) {
            return TaskUtil.getTimeByTask(task) + " 服用时间到了！请在服用后，打开KIGI完成计划。";
        } else {
            return "您设置的 " + task.getContent() + " 提醒时间到，请打开KIGI立即查看!";
        }
    }
}
