package com.example.local_push.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import com.example.local_push.data.DataStore;
import com.example.local_push.receiver.PushReceiver;
import com.example.local_push.worker.AlarmWorker;
import com.example.local_push.worker.WorkerUtil;

import java.util.Calendar;
import java.util.List;

import androidx.core.app.AlarmManagerCompat;

import static android.content.Context.ALARM_SERVICE;

public class AlarmUtil {
    public static String action = "setAlarm";

/*
    public static void setTime(Context context, Calendar calendar, int requestCode) {
//        AlarmManager alarmService = (AlarmManager) context.getSystemService(ALARM_SERVICE);
//        Intent alarmIntent = new Intent(context, PushReceiver.class).setAction(action);
//        if (android.os.Build.VERSION.SDK_INT >= 12) {
//            alarmIntent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);//3.1以后的版本需要设置Intent.FLAG_INCLUDE_STOPPED_PACKAGES
//        }
//        PendingIntent broadcast = PendingIntent.getBroadcast(context, requestCode, alarmIntent,  0);//通过广播接收
//        alarmService.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
//        LogUtil.print("calendar:" + DateUtil.getCalendarStr(calendar) + ",requestCode:" + requestCode);
//
//        if (Build.VERSION.SDK_INT >= 23) {
//            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
//                    triggerTime, pendingIntent);
//        } else if (Build.VERSION.SDK_INT >= 19) {
//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, triggerTime, pendingIntent);
//        } else {
//            alarmManager.set(AlarmManager.RTC_WAKEUP, triggerTime, pendingIntent);
//        }

        int startMillis = (int) calendar.getTimeInMillis();
        Intent alarm = new Intent(context, PushReceiver.class);
        alarm.setAction(action);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, requestCode, alarm, PendingIntent.FLAG_UPDATE_CURRENT);
//        int clock  = AlarmManager.RTC_WAKEUP;
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        AlarmManagerCompat.setAlarmClock(manager, startMillis, pendingIntent, pendingIntent);
//        LogUtil.print("sdk version:" + Build.VERSION.SDK_INT);
//        if (Build.VERSION.SDK_INT >= 23) {
//            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
//                    startMillis, pendingIntent);
//        } else if (Build.VERSION.SDK_INT >= 19) {
//            manager.setExact(AlarmManager.RTC_WAKEUP, startMillis, pendingIntent);
//        } else {
//            manager.set(AlarmManager.RTC_WAKEUP, startMillis, pendingIntent);
//        }
//        if (Build.VERSION.SDK_INT < 19) {
//            manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        } else {
//            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) //Android 6，针对省电优化
//            manager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //Android 4.4，针对set不准确
//            manager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        else
//            manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis(), pendingIntent);
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        boolean allowWhileIdle = false;
//        if (allowWhileIdle) {
//            AlarmManagerCompat.setExactAndAllowWhileIdle(manager, clock, startMillis, pendingIntent);
//        } else {
//            AlarmManagerCompat.setExact(manager, clock, startMillis, pendingIntent);
//        }

    }

    public static void setTimes(Context context, List<Calendar> calendars) {
//            return;
        for (int i = 0; i < DataStore.getInstance().getAlarmCodes(); i++) {
            cancel(context, i);
            LogUtil.print("cancel code:" + i);
        }
//        return;
        DataStore.getInstance().setAlarmMaxCode(calendars.size());
        Calendar nowCalendar = Calendar.getInstance();
        int nowDays = (int) DateUtil.getDaysByCalendar(nowCalendar);
        for (int i = 0; i < calendars.size(); i++) {
            Calendar calendar = calendars.get(i);
            if (DateUtil.getDaysByCalendar(calendars.get(i)) == nowDays) {
                if (DateUtil.getTimeByCalendar(calendar) == DateUtil.getTimeByCalendar(nowCalendar)) {
                    LogUtil.print(DateUtil.getCalendarStr(calendar) + "相同时间不设置闹钟");
                    continue;
                }
            }
            setTime(context, calendars.get(i), i);
        }
    }


    public static void cancel(Context context, int requestCode) {
        Intent intent = new Intent(context, PushReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        if (am != null)
            am.cancel(pi);
    }

*/

    public static void setTime(Context context, Calendar calendar, int requestCode) {

    }

    public static void setTimes(Context context, List<Calendar> calendars) {
        long now = Calendar.getInstance().getTimeInMillis();
        WorkerUtil.setTimes(context, calendars);

    }

    public static void cancel(Context context, int requestCode) {

    }
}
