package com.example.local_push.util;

import com.example.local_push.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {
    public static long getDaysByCalendar(Calendar calendar) {
        long days = (calendar.getTimeInMillis() + calendar.getTimeZone().getRawOffset()) / (24 * 60 * 60000);
        return days;
    }

    public static int getTimeByCalendar(Calendar calendar) {
        return (int) calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
    }

    public static Calendar getCalendarByDays(long days) {
        long s = days * (24 * 60 * 60000) - Calendar.getInstance().getTimeZone().getRawOffset();
//        long s = days * (24 * 60 * 60000);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(s);
        return calendar;
    }

    public static String getCalendarStr(Calendar calendar) {
        String pattern = "MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }

    public static String getCalendarStr2(Calendar calendar) {
        String pattern = "HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }
    public static String getDateFormatStr(Calendar calendar,String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }

    public static long getMinuterByTime(long timeInMillis) {
        return timeInMillis / 60 / 1000;
    }

//    public static long getTimeByCalendar(Calendar calendar) {
//        long date = (calendar.getTimeInMillis() + calendar.getTimeZone().getRawOffset()) / (24 * 60 * 60000);
//        return date;
//    }
//
//    public static Calendar getCalendarByTimes(long days) {
//        long s = days * (24 * 60 * 60000) - Calendar.getInstance().getTimeZone().getRawOffset();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(s);
//        return calendar;
//    }

}
