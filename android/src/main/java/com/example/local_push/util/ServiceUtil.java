package com.example.local_push.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.example.local_push.service.MyIntentJobService;

import java.util.List;

public class ServiceUtil {
    public static boolean isServiceRunning(Context mContext, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager
                .getRunningServices(10000);
        if (!(serviceList.size() > 0)) {
            return false;
        }
//        Log.e("OnlineService：", className);
        for (int i = 0; i < serviceList.size(); i++) {
//            Log.e("serviceName：", serviceList.get(i).service.getClassName());
            if (serviceList.get(i).service.getClassName().contains(className) == true) {
                isRunning = true;
//                Log.e("serviceName：", "break:     " + serviceList.get(i).service.getClassName());
                break;
            }
        }
        return isRunning;
    }

    public static void startService(Context context, Class serviceCls) {
        LogUtil.print("Start service!!!!");
        Intent intent = new Intent(context, serviceCls);
        Bundle bundle = new Bundle();
        bundle.putString("data0", "这是发送给service的话");
//        context.stopService(new Intent(context,serviceCls));
//        if (!isServiceRunning(context, serviceCls.getName())) {
//        context.startService(intent);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            context.startForegroundService(intent);
//        } else {
//            context.startService(intent);
//        }
        MyIntentJobService.startService(context, new Intent());
    }
}
