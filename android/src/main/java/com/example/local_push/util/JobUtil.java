package com.example.local_push.util;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import com.example.local_push.service.JobAlarmService;

import androidx.annotation.RequiresApi;

public class JobUtil {
    public static void run(Context context) {
        JobScheduler jobScheduler = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//
//            jobScheduler.cancelAll();
            JobInfo.Builder builder = new JobInfo.Builder(1024, new ComponentName(context.getPackageName(), JobAlarmService.class.getName()));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //android N之后时间必须在15分钟以上
//            builder.setMinimumLatency(10 * 1000);
                LogUtil.print(">=N");
                builder.setPeriodic(15 * 60 * 1000);
            } else {
                LogUtil.print("<N");
                builder.setPeriodic(60 * 1000);
            }

            builder.setPersisted(true);
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
            int schedule = jobScheduler.schedule(builder.build());
            LogUtil.print("run!!!!!!!!");
            if (schedule <= 0) {
//            Log.w(TAG, "schedule error！");
            }
        }
    }
}
