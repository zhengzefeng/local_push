package com.example.local_push.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import com.example.local_push.LaunchActivity;
import com.example.local_push.LocalPushPlugin;
import com.example.local_push.R;

import java.util.Calendar;

import androidx.core.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationChannels {
    public final static String CRITICAL = "critical";
    public final static String IMPORTANCE = "importance";
    public final static String DEFAULT = "default";
    public final static String LOW = "low";
    public final static String MEDIA = "media";

    public static void show(Context context, String title, String content) {
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//        LogUtil.print("class name:"+context.getClass().getName());
//        context.getApplicationContext().getde
        Intent intent = new Intent(context, LaunchActivity.class);
        int requestCode = (int) System.currentTimeMillis();

        PendingIntent pi = PendingIntent.getActivity(context, requestCode, intent, 0);
//
        String contentTitle = title + " -> " + DateUtil.getDateFormatStr(Calendar.getInstance(), "HH:mm:ss"); //标题
        String contentText = content;//内容
//        NotificationManagerCompat compat=new NotificationManagerCompat

//        Notification notification = new NotificationCompat.Builder(context, "default").setContentTitle(contentTitle)
//                .setContentText(contentText).setWhen(System.currentTimeMillis()).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true)
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher)).build();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "default").setContentTitle(contentTitle)
                .setContentText(contentText).setWhen(System.currentTimeMillis()).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true).setContentIntent(pi);
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelID = "1";
            String channelName = "深度提醒";
            NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
            //创建通知时指定channelID
            builder.setChannelId(channelID);
        }
        Notification notification = builder.build();
        manager.notify(requestCode, notification);

//        String id = "my_channel_01";
//        String name = "我是渠道名字";
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//        Notification notification = null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_LOW);
//            Toast.makeText(context, mChannel.toString(), Toast.LENGTH_SHORT).show();
//            notificationManager.createNotificationChannel(mChannel);
//            notification = new Notification.Builder(context)
//                    .setChannelId(id)
//                    .setContentTitle("5 new messages")
//                    .setContentText("hahaha")
//                    .setSmallIcon(R.drawable.ic_launcher).build();
//        } else {
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                    .setContentTitle("5 new messages")
//                    .setContentText("hahaha")
//                    .setSmallIcon(R.drawable.ic_launcher)
//                    .setOngoing(true)
//                    .setChannelId(id);
////                    .setChannel(id);//无效
//            notification = notificationBuilder.build();
//        }
//        notificationManager.notify(111123, notification);notification


    }


    public static Notification createNotificationChannel(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//
        String contentTitle = "前台服务标题"; //标题
        String contentText = "前台服务内容";//内容
//        NotificationManagerCompat compat=new NotificationManagerCompat

//        Notification notification = new NotificationCompat.Builder(context, "default").setContentTitle(contentTitle)
//                .setContentText(contentText).setWhen(System.currentTimeMillis()).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true)
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher)).build();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "default").setContentTitle(contentTitle)
                .setContentText(contentText).setWhen(System.currentTimeMillis()).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
        int requestCode = (int) System.currentTimeMillis();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelID = "1";
            String channelName = "通知测试";
            NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
            //创建通知时指定channelID
            builder.setChannelId(channelID);
        }
        Notification notification = builder.build();
        return notification;
    }


}
