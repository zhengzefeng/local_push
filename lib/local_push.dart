import 'dart:async';
import 'dart:ffi';

import 'package:flutter/services.dart';

class LocalPush {
  static const MethodChannel _channel = const MethodChannel('local_push');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

//  static Future<void> updateFreshTip(
//      String jsonStr, String todayTasksStr, String boxesStr) async {
//    await _channel.invokeMethod('updateFreshTip', <String, dynamic>{
//      'message': jsonStr,
//      "tasks": todayTasksStr,
//      "boxes": boxesStr
//    });
//  }
//
//  static Future<void> updateSchedulerTip(
//      String schedulersStr, String todayTasksStr, String boxesStr) async {
//    await _channel.invokeMethod('updateSchedulerTip', <String, dynamic>{
//      'schedulers': schedulersStr,
//      "tasks": todayTasksStr,
//      "boxes": boxesStr
//    });
//  }

  static Future<void> updateData(String schedulersStr, String freshTasksStr,
      String todayTasksStr, String boxesStr) async {
    print("Future updateData");
    await _channel.invokeMethod('updateData', <String, dynamic>{
      'freshTasks': freshTasksStr,
      'schedulers': schedulersStr,
      "tasks": todayTasksStr,
      "boxes": boxesStr
    });
  }

  static Future<void> enableLocalPush(bool enableLocalPush) async {
    var enableLocalPushStr = enableLocalPush ? "1" : "0";
    await _channel.invokeMethod('enableLocalPush', <String, dynamic>{
      'enableLocalPush': enableLocalPushStr,
    });
  }
}
