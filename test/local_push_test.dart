import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:local_push/local_push.dart';

void main() {
  const MethodChannel channel = MethodChannel('local_push');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await LocalPush.platformVersion, '42');
  });
}
